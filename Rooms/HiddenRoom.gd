extends TileMap

var shown
var activating
var leftEnter
var onEnter

func _ready():
	shown = true
	activating = false
	leftEnter = true
	set_process(true)

#Set the position in tiles for the entrance collision shape and direction.
func setTriggerTile(newPosition, newDirection):
	get_node("Area2D").set_pos(Vector2(newPosition.x * 48, newPosition.y * 48))
	leftEnter = newDirection

func _process(delta):
	#If it overlaps with something.
	if(get_node("Area2D").get_overlapping_bodies().size() != 0):
		#Iterates through the overlapping bodies.
		for body in get_node("Area2D").get_overlapping_bodies():
			#If overlaps with player.
			if(body.get_name() == "Player"):
				#Depends on which direction is set as entrance.
				onEnter = body.get_pos().x <= get_node("Area2D").get_pos().x if leftEnter else body.get_pos().x >= get_node("Area2D").get_pos().x
				#If onEnter and shown than fade out else fade in.
				if(onEnter):
					if(shown):
						get_node("AnimationPlayer").play("Disappear")
						shown = false
				else: 
					if(!shown):
						get_node("AnimationPlayer").play("Appear")
						shown = true