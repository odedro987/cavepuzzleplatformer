extends StaticBody2D

var isOn
var activating

#Swtitchs the door depending on its state.
func switchObject():
	#If not activating.
	if(!activating):
		#If the door is on, switch the door off.
		if(isOn): 
			get_node("AnimationPlayer").play("Fall")
			get_tree().get_root().get_node("MainScreen").shakeCamera(0.5, 4, 2)
			isOn = false
		#If the door is off, switch the door on.
		else: 
			get_node("AnimationPlayer").play("Rise")
			get_tree().get_root().get_node("MainScreen").shakeCamera(0.8, 1, 1)
			isOn = true
	activating = true

#Resets the door.
func resetDoor():
	activating = false

func getState():
	return isOn

func _ready():
	isOn = false
	activating = false