extends Node2D

var links
var isOn
var activating
var leverIndex

signal switch_callback

#Swtitchs the lever depending on its state.
func switchLever():
	#If not activating.
	if(!activating):
		#If the lever is on, switch the lever off.
		if(isOn): 
			get_node("AnimationPlayer").play("SwitchOff")
			isOn = false
		#If the lever is off, switch the lever on and call the links callback functions.
		else: 
			get_node("AnimationPlayer").play("SwitchOn")
			isOn = true
			#Calls links.
			for object in links:
				if(object.has_method("switchObject")):
					object.switchObject()
			emit_signal("switch_callback")
	activating = true
#Resets the lever.
func resetLever():
	activating = false
#Returns type.
func getType():
	return "Lever"
#Sets the lever links.
func setLinks(newLinks):
	links = newLinks
#Sets the lever type.
func setLeverIndex(newIndex):
	leverIndex = newIndex
	setFrame(0)
#Sets region rect depending on the frame.
func setFrame(frame):
	get_node("Sprite").set_region_rect(Rect2(48 * frame, 48 * leverIndex, 48, 48))

func _ready():
	links = []
	isOn = false
	activating = false
	leverIndex = 0
	set_z(1)