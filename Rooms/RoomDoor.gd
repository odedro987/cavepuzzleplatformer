extends Node2D

var nextRoomIndex
var nextDoorIndex
var index

func _ready():
	nextRoomIndex = -1
	nextDoorIndex = -1
	index = -1
#Return the type.
func getType():
	return "Door"
#Set next room and door.
func setNextDoorInRoom(newNextRoomIndex, newNextDoorIndex):
	nextRoomIndex = newNextRoomIndex
	nextDoorIndex = newNextDoorIndex
#Set a new index.
func setDoorIndex(newIndex):
	index = newIndex
#Initiate door.
func setDoor(newIndex=0, newNextRoomIndex=-1, newNextDoorIndex=-1):
	setDoorIndex(newIndex)
	setNextDoorInRoom(newNextRoomIndex, newNextDoorIndex)