extends Camera2D

#Shake variables.
var shake
var shakeTimer
var shakeCount
var shakeRange
var shakePower
#Follow variables.
var isTranstion
var followSpeed
var followingNode

#Shakes camera for [1] seconds, range of [2] and power of [3].
func shakeCamera(time=1, shakeRange=1, shakePower=1):
	self.shake = true
	self.shakeTimer = time
	self.shakeRange = shakeRange
	self.shakePower = shakePower

func _ready():
	#Randomize.
	randomize()
	shake = false
	shakeTimer = 0
	shakeCount = 0
	shakeRange = 1
	shakePower = 1
	isTranstion = false
	followSpeed = 1
	followingNode = null
	set_process(true)

#Sets the camera limits.
func setAllLimits(left, top, right, bottom):
	set_limit(0, left)
	set_limit(1, top)
	set_limit(2, right)
	set_limit(3, bottom)

#Sets new node to follow.
func setFollowingNode(node):
	self.followingNode = node
	if(!isTranstion):
		set_global_pos(followingNode.get_global_pos())

func transitionTo(node, speed):
	isTranstion = true
	setFollowingNode(node)
	followSpeed = speed

func _process(delta):
	#If shakes.
	if(shake):
		#Start counter.
		shakeCount += delta
		#Set offset to a linear interpolation of the current position and a random factor.
		set_offset(Vector2(lerp(get_pos().x, rand_range(-shakeRange, shakeRange), shakePower), \
					       lerp(get_pos().y, rand_range(-shakeRange, shakeRange), shakePower)))
		#Reset timer.
		if(shakeCount >= shakeTimer):
			shakeCount = 0
			shake = false
			set_offset(Vector2(0, 0))
	#Transitions to the node.
	if(isTranstion):
		var posDiff = Vector2(get_global_pos().x - followingNode.get_global_pos().x, 
							  get_global_pos().y - followingNode.get_global_pos().y)
		#Resets camera when close enough to target.
		if(abs(posDiff.x) < 2 && abs(posDiff.y) < 2):
			isTranstion = false
		#Lerps.
		set_global_pos(Vector2(lerp(get_global_pos().x, followingNode.get_global_pos().x, followSpeed),
							   lerp(get_global_pos().y, followingNode.get_global_pos().y, followSpeed)))
	else:
		#Follows the node.
		if(followingNode != null):
			set_global_pos(followingNode.get_global_pos())