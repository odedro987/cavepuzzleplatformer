extends Node2D

var healthCrystals = 6
var currentHealthCrystals = 6
var fullCrystal = preload("res://Assets/GUI/HealthCrystal.png")
var halfCrystal = preload("res://Assets//GUI//HealthCrystalHalf.png")
var crystalNodes
var size

func _ready():
	crystalNodes = [get_node("Sprite/Crystal1"), get_node("Sprite/Crystal2"), get_node("Sprite/Crystal3")]
	size = Vector2(get_node("Sprite").get_texture().get_width(), get_node("Sprite").get_texture().get_height())
	set_process(true)

#Updates the GUI's crystals.
func updateHealthCrystals(newHealthCrystals):
	#Resets all the crystals.
	for i in range(crystalNodes.size()):
		crystalNodes[i].set_texture(null)
	#Sets the full crystals.
	for i in range(newHealthCrystals / 2):
		crystalNodes[i].set_texture(fullCrystal)
	#If there's supposed to be a half, set it.
	if(ceil(newHealthCrystals / 2.0) != (newHealthCrystals / 2.0)):
		crystalNodes[floor(newHealthCrystals / 2.0)].set_texture(halfCrystal)

func _process(delta):
	#Checks for a change in health, and updates the GUI.
	if(currentHealthCrystals != healthCrystals):
		currentHealthCrystals = healthCrystals
		updateHealthCrystals(currentHealthCrystals)