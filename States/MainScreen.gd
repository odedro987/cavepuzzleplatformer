extends Node

var currentRoomIndex
var currentDoorIndex

func _ready():
	currentRoomIndex = 0
	currentDoorIndex = 0
	loadRoom(currentRoomIndex, currentDoorIndex)
	get_node("Camera2DFX").setFollowingNode(get_node("Player"))

#Shakes the camera.
func shakeCamera(time, shakeRange, shakePower):
	get_node("Player/Camera2DFX").shakeCamera(time, shakeRange, shakePower)

func fadeInOut(roomIndex, doorIndex):
	currentRoomIndex = roomIndex
	currentDoorIndex = doorIndex
	get_node("HUD/ScreenFade").get_node("AnimationPlayer").play("FadeInOut")

func enablePlayerControl():
	get_node("Player").currentState = get_node("Player").States.NORMAL

#Loads a room by its index.
func loadRoom(roomIndex=currentRoomIndex, doorIndex=currentDoorIndex):
	#Removes all CurrentRoom's children.
	for node in range(get_node("CurrentRoom").get_child_count()):
		get_node("CurrentRoom").remove_child(get_node("CurrentRoom").get_child(0))
	#Removes previous parallax layer.
	if(get_node("ParallaxBackground/ParallaxLayer").get_child_count() > 0):
		get_node("ParallaxBackground/ParallaxLayer").remove_child(get_node("ParallaxBackground/ParallaxLayer").get_child(0))
	#Loads and instantiates RoomBank.
	var roomBank = load("res://Rooms/RoomBank.tscn")
	var room = roomBank.instance()
	#Copies the wanted room to CurrentRoom and shows it.
	get_node("CurrentRoom").add_child(room.get_child(roomIndex).duplicate())
	var nextRoom = get_node("CurrentRoom").get_child(0)
	nextRoom.show()
	for node in get_node("CurrentRoom").get_child(0).get_children():
		if(node.get_name() == "Parallax"):
			get_node("ParallaxBackground/ParallaxLayer").add_child(node.duplicate());
			get_node("CurrentRoom").get_child(0).remove_child(node)
	nextRoom.initRoom()
	for node in range(nextRoom.get_child_count()):
		if(nextRoom.get_child(node).has_method("getType")):
			if(nextRoom.get_child(node).getType() == "Door"):
				if(nextRoom.get_child(node).index == doorIndex):
					#Sets the player position to the new room's entrance door position.
					get_node("Player").set_pos(nextRoom.get_child(node).get_pos())
					get_node("Player").resetVelocity()
	#Sets the camera's limits to the cuurent map size.
	var tilemapRect = nextRoom.get_node("Map/TileMap").get_used_rect()
	get_node("Camera2DFX").setAllLimits(tilemapRect.pos.x * 48, tilemapRect.pos.y * 48,
											   tilemapRect.end.x * 48, tilemapRect.end.y * 48)