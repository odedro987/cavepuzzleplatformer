extends KinematicBody2D
#Force that pulls you down.
const GRAVITY = 1300.0
#Running max speed.
const MAX_SPEED = 340
#Acceleration.
const ACCELERATION = 800
#Deceleration when stop moving.
const DECELERATION = 1500
#Deceleration when changing directions.
const DIRECTION_MOMENTUM_FORCE = 1500
#Charge jump factor.
const CHARGE_JUMP_FACTOR = 2
#Small jump speed.
const SMALL_JUMP_SPEED = 350
#Time between jumps.
const JUMP_INTERVAL_LIMIT = 0.15
#Time until you can move again from a wall jump.
const WALLJUMP_SUSPENSION_LIMIT = 0.25
#Time until charge jump ends.
const MAX_CHARGE_JUMP_TIME = 0.2
#Force that pushes you from the wall.
const WALLJUMP_PUSH_FORCE_UP = 600
#Force that pushes you from the wall.
const WALLJUMP_PUSH_FORCE_SIDE = 400
#Time for hit-invincibility.
const INVINCIBLE_LIMIT = 1
#Time until movement is enabled after knockback.
const KNOCKBACK_MOVEMENT_LIMIT = 0.5
#Flickering rate on hurt.
const HURT_FLICKER_RATE = 4
#Time until can enter a door again.
const INTERACTION_TIME_LIMIT = 1
###########MOVEMENT VARIABLES###########
var velocity = Vector2(0, 0)
var acceleration = Vector2(0, 0)
var motion
var normalize
var direction = 1
var canJump = true
var wallJumpSuspension = 0.3
var wallRayDirection = 1
var moveButton = "None"
var canMove = true
#Jumps
var onGround = false
var onWall = false
var wallJumping = false
var wallJumpHeight = false
var jumpChargeTime = 0
var chargingJump = false
var timeSinceLastJump = 0.0
############DAMAGE VARIABLES############
var health = 6.0
var isDamageDealt = false
var invincibleTime = 0.0
var invincible = false
#########INTERCATABLES VARIABLES########
var canInteract = true
var interactTimer = 0
#############OTHER VARIABLES############
enum States { NORMAL, HURT, WALLJUMP, DISABLED }
var currentState
var currentAnimation = "None"
var sprite
var animationPlayer
var wallRay
var floorRay
#Constructor.
func _ready():
	set_opacity(2.0)
	currentState = States.NORMAL
	sprite = get_node("Sprite")
	animationPlayer = get_node("AnimationPlayer")
	wallRay = get_node("WallRay")
	floorRay = get_node("FloorRay")
	set_fixed_process(true)
#Applies knockback.
func knockback(force, newDirection):
	resetVelocity()
	newDirection = newDirection if newDirection != 0 else -direction
	velocity = Vector2(force.x * newDirection, -force.y)
	canMove = false
	onGround = false
#Applies walljump push.
func wallJump():
	velocity = Vector2(WALLJUMP_PUSH_FORCE_SIDE * -direction, -WALLJUMP_PUSH_FORCE_UP)
#Decreases health.
func damagePlayer(damageDealt, knockbackForce, newDirection):
	if(!invincible):
		#Sets invincible and isDamageDealt to true.
		invincible = true
		isDamageDealt = true
		#Disable movement and reset jumping.
		canMove = false
		chargingJump = false
		wallJumping = false
		#Change state to HURT.
		currentState = States.HURT
		#Play animation.
		animationPlayer.play("Idle")
		currentAnimation = "Idle"
		#Decrease health.
		health -= damageDealt
		#Knocks back.
		knockback(knockbackForce, newDirection)
		#Checks if dead.
		if(health < 0.0):
			#TODO: Death stuff.
			get_tree().get_root().get_node("MainScreen/HUD/HealthGUI").updateHealthCrystals(0)
		else:
			get_tree().get_root().get_node("MainScreen/HUD/HealthGUI").updateHealthCrystals(health)
#Updates the direction of the wall raycasting.
func updateWallRayCast(newDirection):
	wallRay.set_pos(Vector2(21 * newDirection, 0))
	wallRay.set_cast_to(Vector2(2 * newDirection, 0))
	wallRayDirection = newDirection
	onWall = false
	wallRay.force_raycast_update()
#Update function.
func _fixed_process(delta):
	#Interact timer.
	if(!canInteract):
		interactTimer += delta
		if(interactTimer >= INTERACTION_TIME_LIMIT):
			interactTimer = 0
			canInteract = true
	#Invincibility timer.
	if(invincible):
		invincibleTime += delta
		if(invincibleTime >= INVINCIBLE_LIMIT):
			#Resets variables.
			isDamageDealt = false
			invincibleTime = 0.0
			invincible = false
			set_opacity(2.0)
	#Normal state.
	if(currentState == States.NORMAL):
		movementInput(delta)
		jumpInput(delta)
		interactInput(delta)
		if(wallJumping): currentState = States.WALLJUMP
	#Hurt state.
	elif(currentState == States.HURT):
		damageInput(delta)
		#Applys gravity.
		velocity.y += GRAVITY * delta
		#Calcualtes the direction of the knockback force(the sign +/-).
		var hitDirection = velocity.x / abs(velocity.x)
		#Decelerates to the opposite direction of the hit.
		velocity.x = 0 if abs(velocity.x) - DECELERATION * delta < 0 else velocity.x + DECELERATION * delta * -hitDirection
	elif(currentState == States.WALLJUMP):
		jumpInput(delta)
		#Calcualtes the direction of the knockback force(the sign +/-).
		var hitDirection = velocity.x / abs(velocity.x)
		#Decelerates to the opposite direction of the hit.
		velocity.x = 0 if abs(velocity.x) - DECELERATION * delta < 0 else velocity.x + DECELERATION * delta * -hitDirection
	elif(currentState == States.DISABLED):
		#Return to idle state.
		if(currentAnimation != "Idle"): 
			get_node("AnimationPlayer").play("Idle")
			currentAnimation = "Idle"
		#Applys gravity.
		velocity.y += GRAVITY * delta
		#Stops player.
		velocity.x = 0
	#Move by velocity separately.
	motion = velocity * delta
	move(Vector2(motion.x, 0))
	move(Vector2(0, motion.y))
#Movement checks.
func movementInput(delta):
	#Reseting acceleration.
	acceleration.x = 0
	#Checking if pressed movement buttons.
	if((Input.is_action_pressed("ui_left") || Input.is_action_pressed("ui_right")) && canMove):
		#If left.
		if(Input.is_action_pressed("ui_left")):
			#Flipping to the left.
			sprite.set_flip_h(true)
			#Setting direction to run and left respectively.
			direction = -1
			moveButton = "Left"
			#Decelerating if suddenly changes direction.
			if(velocity.x > 0): velocity.x = 0 if abs(velocity.x) - DIRECTION_MOMENTUM_FORCE * delta < 0 else velocity.x - DIRECTION_MOMENTUM_FORCE * delta
		#If right.
		else:
			#Flipping to the left.
			sprite.set_flip_h(false)
			#Setting direction to run and left respectively.
			direction = 1
			moveButton = "Right"
			#Decelerating if suddenly changes direction.
			if(velocity.x < 0): velocity.x = 0 if abs(velocity.x) + DIRECTION_MOMENTUM_FORCE * delta < 0 else velocity.x + DIRECTION_MOMENTUM_FORCE * delta
		#Accelerating.
		acceleration.x = ACCELERATION * direction
		#Updates raycast.
		if(wallRayDirection != direction): updateWallRayCast(direction)
		#As long as Run animation isn't playing. 
		if(currentAnimation != "Run" && currentAnimation != "Jump" && currentAnimation != "Fall"):
			#Playing Run animation.
			animationPlayer.play("Run")
			currentAnimation = "Run"
	else:
		moveButton = "None"
		#As long as Idle animation isn't playing. 
		if(currentAnimation != "Idle" && currentAnimation != "Jump" && currentAnimation != "Fall" && currentAnimation != "Land"):
			#Playing Idle animation and sets anim to idle.
			animationPlayer.play("Idle")
			currentAnimation = "Idle"
		#Decelerating and sliding(according to velocity and not sprite direction).
		velocity.x = 0 if abs(velocity.x) - DECELERATION * delta < 0 else velocity.x + DECELERATION * delta * -(velocity.x / abs(velocity.x))
	#Setting velocity to acceleration time delta.
	velocity.x = velocity.x + acceleration.x * delta if abs(velocity.x) < MAX_SPEED else MAX_SPEED * direction
#Jump checks.
func jumpInput(delta):
	#Check raycasts.
	if(wallRay.is_colliding()):
		#If collides with walls.
		if(wallRay.get_collider().get_type() == "TileMap"):
			onWall = true
	else:
		onWall = false
	#WallJumpHeight is the opposite of whether the floorRay is colliding. If floorRay is colliding then the player is not high enough.
	wallJumpHeight = !floorRay.is_colliding()
	if(is_colliding()):
		normalize = get_collision_normal()
		#If touching ground.
		if(normalize.y < 0):
			#Reset volocity.y.
			velocity.y = 0
			#Reset onGround.
			if(!onGround): onGround = true
			#If on the ground and currentAnimation isn't idle or running set as none.
			if(currentAnimation != "Idle" && currentAnimation != "Run" && currentAnimation != "Land"): 
				get_node("AnimationPlayer").play("Land")
				currentAnimation = "Land"
		#If touching ceiling reset velocity.y.
		if(normalize.y > 0): velocity.y = 0
		#If touching walls reset velocity.x.
		if(onWall): velocity.x = 0
	else:
		#Resets onGround if not colliding.
		if(onGround): onGround = false
		#If falling, meaning current position is lower than your position in the next frame(current position + velocity.y).
		if(get_pos().y < get_pos().y + velocity.y * delta):
			if(currentAnimation != "Fall"):
				get_node("AnimationPlayer").play("Fall")
				currentAnimation = "Fall"
	#Enabling gravity, if wall jumping disable it.
	if(!wallJumping || !onGround): velocity.y += GRAVITY * delta
	#If pressed up.
	if(Input.is_action_pressed("ui_up") && canMove):
		#If player is on ground.
		if(onGround):
			#Set onGround to false and start chargingJump.
			onGround = false
			chargingJump = true
			#Play jump animation.
			get_node("AnimationPlayer").play("Jump")
			currentAnimation = "Jump"
		#If touches wall, not wall jumping, high enough to wall jump and moving in a direction.
		elif(onWall && !wallJumping && wallJumpHeight && moveButton != "None"):
			#Start wall jump.
			wallJumping = true
			#Disable movement.
			canMove = false
			#Resets velocity.
			resetVelocity()
			#Push from wall.
			wallJump()
			#Play jump animation. |||||||| TODO: Make a wall jump animation.
			get_node("AnimationPlayer").play("Walljump")
			currentAnimation = "Jump"
	#If up key is released reset chargingJump.
	else:
		if(chargingJump): 
			chargingJump = false
			jumpChargeTime = 0
	#If chargingJump.
	if(chargingJump):
		#Starts timer.
		jumpChargeTime += delta
		#Adds a fraction of small speed jump to the small jump.
		velocity.y = -SMALL_JUMP_SPEED - (SMALL_JUMP_SPEED * jumpChargeTime * CHARGE_JUMP_FACTOR)
		#if timer is up, reset it.
		if(jumpChargeTime >= MAX_CHARGE_JUMP_TIME): 
			chargingJump = false
			jumpChargeTime = 0
	#If wallJumping.
	if(wallJumping):
		#Start timer.
		timeSinceLastJump += delta
		#If timer is up.
		if(timeSinceLastJump >= WALLJUMP_SUSPENSION_LIMIT):
			#Reset variables.
			wallJumping = false
			timeSinceLastJump = 0
			velocity.x = 0
			canMove = true
			currentState = States.NORMAL
#Interact checks.
func interactInput(delta):
	#Checks for doors if the down arrow is clicked.
	if(Input.is_action_pressed("ui_down") && onGround):
		if(get_node("ActivationArea").get_overlapping_bodies().size() != 0):
			#Iterates through the overlapping bodies.
			for body in get_node("ActivationArea").get_overlapping_bodies():
				#Ignores player's collision shape.
				if(body.get_name() != "Player" && body.get_name() != "TileMap"):
					if(body.has_method("getType")):
						#Checks for doors.
						if(body.getType() == "Door"):
							#If there's a nextRoomIndex and timer is over.
							if(body.nextRoomIndex != -1 && canInteract):
								canInteract = false
								#Calls MainScreen's loadRoom function.
								get_tree().get_root().get_node("MainScreen").fadeInOut(body.nextRoomIndex, body.nextDoorIndex)
								#Disables player's control.
								currentState = States.DISABLED
						#Checks for levers.
						elif(body.getType() == "Lever"):
							#Switch the lever depending on its state.
							body.switchLever()
#Damage checks.
func damageInput(delta):
	#Checks if damage is dealt, if so start hit-invincibility.
	if(isDamageDealt):
		#Starts the invincibility counter.
		set_opacity(get_opacity() + delta * HURT_FLICKER_RATE if get_opacity() < 1 else 0.2)
		#If counter is over.
		if(invincibleTime >= KNOCKBACK_MOVEMENT_LIMIT):
			if(!canMove): canMove = true
			currentState = States.NORMAL
#Resets velocity.
func resetVelocity():
	acceleration = Vector2(0, 0)
	velocity = Vector2(0, 0)