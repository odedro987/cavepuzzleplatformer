extends Node2D

var activating = false
var damage = 1
var knockbackForce = Vector2(400, 160)
var canDamage = false

func _ready():
	set_process(true)
#Starts reset animation.
func resetSpike():
	get_node("AnimationPlayer").play("Reset")
	activating = false
	canDamage = false
#Enable possible damaging.
func enableDamage():
	canDamage = true

func _process(delta):
	#If not mid-animation.
	if(!activating || canDamage):
		#If it overlaps with something.
		if(get_node("Area2D").get_overlapping_bodies().size() != 0):
			#Iterates through the overlapping bodies.
			for body in get_node("Area2D").get_overlapping_bodies():
				#If overlaps with player.
				if(body.get_name() == "Player"):
					if(!body.invincible):
						#Starts animation.
						if(!activating): get_node("AnimationPlayer").play("Activate")
						#Damages the player.
						if(canDamage):
							body.damagePlayer(damage, knockbackForce, 0)
						activating = true