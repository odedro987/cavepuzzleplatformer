extends Node2D

var typeCount
var TYPES_IN_ROW = 4
var damage = 1
var knockbackForce = Vector2(400, 160)

func _ready():
	typeCount = get_node("Sprite").get_hframes() * get_node("Sprite").get_vframes()
	randomize()
	var randomSpriteIndex = int(floor(rand_range(0, typeCount - 0.0001)))
	get_node("Sprite").set_frame(randomSpriteIndex / TYPES_IN_ROW + randomSpriteIndex % TYPES_IN_ROW)
	set_process(true)

func _process(delta):
	#If it overlaps with something.
	if(get_node("Area2D").get_overlapping_bodies().size() != 0):
		#Iterates through the overlapping bodies.
		for body in get_node("Area2D").get_overlapping_bodies():
			#If overlaps with player.
			if(body.get_name() == "Player"):
				if(!body.invincible):
					body.damagePlayer(damage, knockbackForce, 0)