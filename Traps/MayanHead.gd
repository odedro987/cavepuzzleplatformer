extends Node2D

var visionRange
var activating
var direction
var arrow

func _ready():
	arrow = get_node("Arrow")
	direction = 1
	activating = false
	visionRange = 300
	updateDirection(direction)
	set_process(true)

#Updates the direction.
func updateDirection(newDirection):
	direction = newDirection
	get_node("Sprite").set_flip_h(true if direction == -1 else false)
	updateVisionRange(visionRange)
	get_node("Arrow").updateDirection(newDirection)

#Updates vision range.
func updateVisionRange(newVisionRange):
	get_node("Area2D/CollisionShape2D").get_shape().set_extents(Vector2(newVisionRange, 10))
	get_node("Area2D/CollisionShape2D").set_pos(Vector2(newVisionRange * direction, 0))

#Resets state.
func resetHead():
	updateVisionRange(visionRange)
	activating = false

#Initiates shooting arrow.
func shootArrow():
	#Plays shoot animation.
	get_node("AnimationPlayer").play("Shoot")
	#Activating arrow and starts its animation.
	arrow.activate(true)
	arrow.get_node("AnimationPlayer").play("Shoot")

func _process(delta):
	#If not mid-animation.
	if(!activating && !arrow.active):
		#If it overlaps with something.
		if(get_node("Area2D").get_overlapping_bodies().size() != 0):
			#Iterates through the overlapping bodies.
			for body in get_node("Area2D").get_overlapping_bodies():
				#If overlaps with player.
				if(body.get_name() == "Player"):
					if(!body.invincible):
						#Shoots arrow.
						shootArrow()
						activating = true