extends Node2D

#Maximum time an arrow has before deactivation.
const MAX_ACTIVE_TIME = 1
#Speed of the arrow.
var speed
var direction
var velocity
var damage
var knockbackForce
var canMove
var active
var activeTime

func _ready():
	speed = 1000
	direction = 1
	velocity = Vector2(speed * direction, 0)
	damage = 1
	knockbackForce = Vector2(400, 160)
	canMove = false
	active = false
	activeTime = 0
	hide()

#Updates the direction.
func updateDirection(newDirection):
	direction = newDirection
	get_node("Sprite").set_flip_h(true if direction == -1 else false)
	velocity = Vector2(speed * direction, 0)

#Changes the state of the arrow.
func activate(flag):
	#Sets process according to flag.
	set_process(flag)
	active = flag
	#Shows if true.
	if(flag): show()
	#If false, hides and sets the frame to 0. 
	else: 
		hide()
		get_node("AnimationPlayer").stop(true)
		get_node("Sprite").set_frame(0)
	#Resets the position.
	set_pos(Vector2(56 * direction, 0))
	#Disabling movement.
	setMove(false)
	#Resets active arrow time.
	activeTime = 0

#Changes the state of the arrow movement.
func setMove(flag):
	if(active): canMove = flag
	else: canMove = false

func _process(delta):
	#Move if allowed.
	if(canMove): 
		set_pos(get_pos() + velocity * delta)
		#Starts active timer.
		activeTime += delta
		#If active timer ends, deactivate and reset.
		if(activeTime >= MAX_ACTIVE_TIME):
			activate(false)
			activeTime = 0
	#Checks overlapping bodies.
	if(get_node("Area2D").get_overlapping_bodies().size() != 0):
		#Iterates through the overlapping bodies.
		for body in get_node("Area2D").get_overlapping_bodies():
			#If overlaps with player.
			if(body.get_name() == "Player"):
				if(!body.invincible):
					#Damages the player.
					body.damagePlayer(damage, knockbackForce, direction)
			#Deactivating after collision.
			activate(false)