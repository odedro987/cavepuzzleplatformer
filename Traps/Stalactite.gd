extends Node2D

const MAX_VELOCITY = 800
var TYPES_IN_ROW = 4
var typeCount
var active
var landed
var damage
var knockbackForce
var canDamage
var visionRange
var velocity
var sprite

func _ready():
	sprite = get_node("Sprite")
	typeCount = sprite.get_hframes() * sprite.get_vframes() / 2
	active = false
	landed = false
	damage = 2
	knockbackForce = Vector2(260, 160)
	canDamage = false
	visionRange = 120
	velocity = Vector2(0, 0)
	randomize()
	updateVisionRange(visionRange)
	setRandomFrame(false)
	set_fixed_process(true)
#Updates vision range.
func updateVisionRange(newVisionRange):
	get_node("VisionArea/VisionShape2D").get_shape().set_extents(Vector2(10, newVisionRange))
	get_node("VisionArea/VisionShape2D").set_pos(Vector2(0, newVisionRange - 3))

func _fixed_process(delta):
	if(!active):
		if(!landed):
			#If it overlaps with something.
			if(get_node("VisionArea").get_overlapping_bodies().size() != 0):
				#Iterates through the overlapping bodies.
				for body in get_node("VisionArea").get_overlapping_bodies():
					#If overlaps with player.
					if(body.get_name() == "Player"):
						active = true
	else:
		if(!landed):
			velocity.y = velocity.y + MAX_VELOCITY * delta if velocity.y < MAX_VELOCITY else velocity.y
			set_pos(get_pos() + velocity * delta)
			#If it overlaps with something.
			if(get_node("CollisionArea").get_overlapping_bodies().size() != 0):
				#Iterates through the overlapping bodies.
				for body in get_node("CollisionArea").get_overlapping_bodies():
					#If overlaps with player.
					if(body.get_name() == "Player"):
						if(!body.invincible):
							#Damage the player.
							body.damagePlayer(damage, knockbackForce, 0)
					elif(body.get_name() == "TileMap"):
						landed = true
		#After landed on the floor.
		else:
			#Fix position to snap to grid.
			set_pos(Vector2(get_pos().x, floor(get_pos().y / 48) * 48 + 6))
			#Change to random debris.
			setRandomFrame(true)
			#Shake camera.
			get_tree().get_root().get_node("MainScreen").shakeCamera(0.3, 2, 2)
			#Stop checking.
			active = false
#Sets a random sprite frame; if debris adds an offset.
func setRandomFrame(isDebris):
	var offset = TYPES_IN_ROW if isDebris else 0
	var randomSpriteIndex = floor(rand_range(0, typeCount - 0.0001))
	get_node("Sprite").set_frame(offset + randomSpriteIndex / TYPES_IN_ROW + randomSpriteIndex % TYPES_IN_ROW)